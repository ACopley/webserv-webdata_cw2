import requests
from bs4 import BeautifulSoup
import time
import ast
import csv
##Web Services and Web Data Coursework 2 Sourcecode

index = []
baseurl = "http://example.python-scraping.com"
blacklist = []

def add_to_urllist(urls, url):
    if url not in urls:
        urls.append(url)
    return

def meetsrules(url):
    if url == "#" or url == "/places/default/index/0" or url == "places/default/index":
        return False
    for item in blacklist:
        if item in url:
            return False
    return True

def build():
    urls = []
    urls.append(baseurl)
    remove = ',!?#:;.0123456789()-/\+><@[]|'
    for url in urls:
        response = requests.get(url)
        soup = BeautifulSoup(response.content, "html.parser")
        links = soup.find_all('a')
        for link in links:
            if meetsrules(link.get('href')):
                add_to_urllist(urls, baseurl + link.get('href'))
                if "register" in link.get('href'):
                    blacklist.append("register")
                if "login" in link.get('href'):
                    blacklist.append("login")
        pagecontent = soup.body.stripped_strings
        for line in pagecontent:
            words = line.split()
        #    print(words)
            for word in words:
                exists = False
                if word[0] == '^':
                    break
                for item in remove:
                    if item in word:
                        word = word.replace(item, '')
        #        print(word)
                if word != "":
                    for dict in index:
                        if dict['word'] == word:
                        #    print(word + "Exists \n")
                            exists = True
                            break
                    if not exists:
                        index.append({'word': word, 'url': [url], 'count': [1]})
                    else:
                        #print(word)
                        if url in dict['url']:
                        #    print(url, dict['url'])
                            dict['count'][dict['url'].index(url)] += 1
                        else:
                            tempurl = dict['url']
                            tempurl.append(url)
                            dict['url'] = tempurl
                            tempcount = dict['count']
                            tempcount.append(1)
                            dict['count'] = tempcount
                        #    if(word == "Example"):
                        #        print(word + " \n", dict['url'])

        time.sleep(5)
        print(index)
    with open('index.csv', 'w') as out:
        f = csv.DictWriter(out, fieldnames=index[0].keys())
        f.writeheader()
        f.writerows(index)
    return

def load():
    with open('index.csv', 'r') as read:
        f = csv.DictReader(read)
        for row in f:
            url = ast.literal_eval(row['url'])
            count = ast.literal_eval(row['count'])
            index.append({'word': row['word'], 'url': url, 'count': count})
    return

def printit(word):
    if(index == []):
        print("please build or load an index first")
        return
    if word == "":
        print("please supply argument")
        return
    for dict in index:
        if(dict['word'] == word):
            print(dict)

def find(words):
    if(index == []):
        print("please build or load an index first")
        return
    if(words == ""):
        print("please supply an argument")
        return
    tempindex = []
    urls = []
    for word in words:
        for dict in index:
            if(dict['word'] == word):
                tempindex.append(dict)
    for dict in tempindex:
        for url in dict['url']:
            exists = False
            if(urls == []):
                urls.append([1, url])
            else:
                for link in urls:
                    if(url == link[1]):
                        exists = True
                        break
                if exists:
                    link[0] += 1
                else:
                    urls.append([1, url])

    urls.sort(reverse=True)
    for item in urls:
        print(str(item[1]) + " has " + str(item[0]) + " of supplied words")


def main():
    while(True):
        cmd = input(">> ")
        cmd = cmd.split(" ")
        if cmd[0] == "build":
            build()
        elif cmd[0] == "load":
            load()
        elif cmd[0] == "print":
            printit(cmd[1])
        elif cmd[0] == "find":
            cmd.remove("find")
            find(cmd)
        elif cmd[0] == "exit":
            break

main()
